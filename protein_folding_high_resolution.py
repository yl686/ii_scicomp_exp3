import numpy as np
from matplotlib import pyplot as plt
from source import Cell, Protein
import time

# this is *the* main script used to produce the fine-grid protein
# simulation plot. Not recommended to run, as it takes >30 mins.

"""Excercise 3_Task 1, "Protein Folding Simulation"
Each task is divided into three parts:

    1) Create suitable Protein(Cell) object from the corresponding data file.
    2) Apply appropriate method to evolve the cell.
    3) Visualise the output file as graph.

"""

print("Simulating protein folding under different denaturant conc.\n")
my_protein = Protein.gen_from_txt('./input_files/protein.txt')
print("Protein created from protein.txt")
# This urea conc spacing produces the graph to a good resolution
urea_conc = np.concatenate((np.linspace(0.0,3.5,14), np.linspace(3.5,5.5,32),\
    np.linspace(5.5,8.0,14)))
print("Start simulation...")
t0 = time.time()
my_protein.folding_simulation(urea_conc,rel_tol=5e-09)
t1 = time.time()
print("Simulation complete.\nPlotting the graph...")
my_protein.plot(my_protein,figname='protein_folding.png')
print("Graph saved as 'protein_folding.png'.")
print("The simulation took {:.0f} seconds.".format(t1-t0))
