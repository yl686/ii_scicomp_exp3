"""WARNING: this will take *940 seconds* to run.
If you are short of time, change the rel_tol value on line 30 to 5e-06.
"""

import numpy as np
from matplotlib import pyplot as plt
from source import Cell, Protein
import time

# The quick-run script that only differs in the no. datapoints.
# This should take around 900 seconds.

"""Application of class Cell to complete the two tasks in Exercise 3.
Each task is divided into three parts:

    1) Create suitable Cell object from the corresponding data file.
    2) Apply appropriate method to evolve the cell.
    3) Visualise the output file as graph.

"""

print("Simulating protein folding under different denaturant conc.\n")
my_protein = Protein.gen_from_txt('./input_files/protein.txt')
print("Protein created from protein.txt")
# This urea conc spacing produces the graph to a good resolution
urea_conc = np.concatenate((np.linspace(0.0,3.5,7), np.linspace(3.5,5.5,16),\
    np.linspace(5.5,8.0,7)))
print("Start simulation...")
t0 = time.time()
my_protein.folding_simulation(urea_conc,rel_tol=5e-07)
t1 = time.time()
print("Simulation complete.\nPlotting the graph...")
my_protein.plot(figname='protein_folding.png')
print("Graph saved as 'protein_folding.png'.")
print("The simulation took {:.0f} seconds.".format(t1-t0))
