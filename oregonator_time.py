import numpy as np
from matplotlib import pyplot as plt

from source import OregonatorCell

"""
Execution file for time evolution of a uniform oregonator solution.
WARNING: it takes good 100 mins.
Reduce the simestep or the length if you must run.
"""


time_cell = OregonatorCell.gen_from_txt('./input_files/oregonator_time.txt')
time_cell.evolve_time(90,filename='oregonator_time_log.dat',dt=1e-6)

# checkpoint 1:
time_cell.timeplot()
