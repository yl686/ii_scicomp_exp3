# PARTII Scientific Computing Exercise 3_Kinetics Simulation
## Description
Simulation of time evolution of a system by stepwise increment.

The project consists of three parts:
1. Equilibrium behaviour

  by protein folding simulation under different concentrations of denaturant

2. Time evolution tracking

  by simulating "the Oregonator" - which exhibits oscillatory behaviour

3. Kinetic in spetial manifest

  by simulating 1D/3D oregonator with non-uniform concentration distribution as initial condition

## Contents
### Script files
- source.py - contains the classes Cell, Protein, OregonatorCell, Oregonator1D and Oregonator3D

- "execution files":
  - protein_folding_high_resolution.py - produces smooth plots at a cost of a very long execution time
  - protein_folding_demo.py - same code but half the datapoints
  to show that the code actually works
  - oregonator_time.py - simulation of the time evolution of a uniform oregonator system

### Datafiles
- input_files/ - contains all the input .txt files required for execution.
The data have been sourced from the Scientific Computing Handout
- 1_protein/ - expected output files from protein_folding*.py scripts
  - protein_high_resolution.png - plots created from 60 data points and relative tolerance 5e-9
  - protein_folding.png - plots from half data points and rel_tol = 5e-7, which are not as pretty
- 2_oregonator/ - expected output files from oregonator_time.py
- 3_oregonator_1D/ - expected output files from oregonator_xD.py and the sketch of the programme

### Helper files
- git-related:
  - .gitignore
  - .git


- dependency management
  - Pipfile
  - Pipfile.lock


## Design
### Overall structure
*class* `Cell` is the base class for all the objects for simulation based on cell(s)
containing reactant and product species in various concentrations.
There is a list of reactions taking place in a cell, with each described by
the stoichiometry of the reactants and products, and the rate constant.

Most input data are handled by .txt files. In some cases, like `protein.k_urea_fn()`, the impirical
data have been hard-coded in.

Each task has its own *class* (or classes in case of spatial Oregonator), which is self-sufficient for simulation.

### Task 1_protein
#### flow
The system to be simulated is modelled by an instance of *class* `Protein`, which is instantiated with the initial conc. of the species and
reactions with rate constants in absence of denaturant.

`Protein.folding_simulation` initialises the system and calculates the **effective k** with the list of urea conc. to be simulated at.
 Then the method iterates over each urea conc., calculates the equilibrium state for each.
 After the iteration has finished, the results are recorded on a text file.
The text file is remembered by the `Protein` object.

`Protein.plot` produces protein plot from either the file saved by the object or an explicitly stated file.

### Task 2_oregonator
#### flow
*class* `OregonatorCell` is instantiated using `OregonatorCell.gen_from_txt` with no spatial information.
`self.evolve_time` uses the base class method to record time evolution over the given interval.
The results are saved as .dat file, which is remembered by the object.
The data can then be plotted using `self.timeplot`.

### Task 3_spatial Oregonator
#### Design
The system is represented by an instance of *class* `Oregonator1D` or `Oregonator3D`,
which consists of `OregonatorCell` objects with shared information about the reactions taking place.
