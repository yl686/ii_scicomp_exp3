import numpy as np
from matplotlib import pyplot as plt

from source import Oregonator1D

"""
Execution file for 1D spatially resolved Oregonator system.
"""

# Instantiation of the system of 10 cells
initial_state = "./input_files/oregonator_1D_state.txt"
reactions = "./input_files/oregonator_1D_rxn.txt"
oregonator_1d = Oregonator1D.from_txt(initial_state,reactions)
print(oregonator_1d[0].conc["A"])
print(oregonator_1d[0].rate_const)
print(oregonator_1d[0].reactions[0][0])
oregonator_1d.evolve_space(5,"space_test.dat",k_diffusion=1e-10,dt=1e-04)
oregonator_1d.visualise(1,"A")
